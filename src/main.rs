fn main() {
    let mut name: String = String::new();
    let mut last: char = ' ';
    let mut index: usize = 0;
    let mut indexes_to_remove: Vec<usize> = Vec::new();

    std::io::stdin().read_line(&mut name).unwrap();

    for c in name.trim().chars().into_iter() {
        if c == last {
            indexes_to_remove.push(index);
        }
        last = c;
        index += 1;
    }

    for i in indexes_to_remove.iter().rev() {
        name.remove(*i);
    }

    println!("{}", name);
}
